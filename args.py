__author__ = 'ilyakuchumov'

import argparse


class SourceTypes:
    ALL = 'ALL'
    ACM = 'ACM'
    FB = 'FB'
    GCJ = 'GCJ'
    ACM_FILES = 'ACM_FILES'


def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument('name', type=str, help='contest or problem name')

    parser.add_argument('-dn', type=bool, dest='default_naming', nargs='?', const=True,
                        help='use default naming for source in single mode')

    parser.add_argument('-l', '-c', dest='letter', type=str,
                        help=('if option exist kmake will create contest with such number of problems, '
                              'else kmake create single problem'))

    parser.add_argument('-t', dest='source_type', default=SourceTypes.ACM, type=str,
                        choices=[SourceTypes.ACM, SourceTypes.FB, SourceTypes.GCJ, SourceTypes.ACM_FILES],
                        help='type of solutions sources. default = {}'.format(SourceTypes.ACM))

    parser.add_argument('--templates', dest='templates_path', default='./templates', type=str,
                        help='relative path to templates folder')

    parser.add_argument('--matching', dest='sources_matching_path', default='./sources_matching.json', type=str,
                        help='relative path to matching config file')

    args = parser.parse_args()
    return args
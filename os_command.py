__author__ = 'ilyakuchumov'

import os
from os.path import join
import subprocess


class CreateDirCommand:
    def __init__(self, path):
        self.path = path

    def execute(self):
        if os.path.exists(self.path):
            raise ValueError('directory {} already exist'.format(self.path))

        os.makedirs(self.path)

    def expand_path(self, prefix):
        return CreateDirCommand(join(prefix, self.path))

    def __str__(self):
        return 'CreateDirCommand: path = {}'.format(self.path)

    def __repr__(self):
        return self.__str__()


class CreateFileCommand:
    def __init__(self, path, content):
        self.path = path
        self.content = content

    def execute(self):
        if os.path.exists(self.path):
            raise ValueError('file {} already exist'.format(self.path))

        with open(self.path, 'w+', encoding='utf-8') as file:
            file.write(self.content)

    def expand_path(self, prefix):
        return CreateFileCommand(join(prefix, self.path), self.content)

    def __str__(self):
        return 'CreateFileCommand: path = {}\ncontent =\n{}'.format(self.path, self.content)

    def __repr__(self):
        return self.__str__()


class AddPermissionCommand:
    def __init__(self, path):
        self.path = path

    def execute(self):
        if not os.path.exists(self.path):
            raise ValueError('file {} is not exist'.format(self.path))
        subprocess.call(['chmod', '777', self.path])

    def expand_path(self, prefix):
        return AddPermissionCommand(join(prefix, self.path))

    def __str__(self):
        return 'AddPermissionCommand: path = {}'.format(self.path)

    def __repr__(self):
        return self.__str__()

#!/usr/bin/env python3

__author__ = 'ilyakuchumov'

import os
import sys
from os.path import join
import re
import json

from template import Template
from os_command import CreateDirCommand, CreateFileCommand, AddPermissionCommand
from args import parse_args


def get_problems_count(letter):
    if letter is None:
        raise ValueError('letter is None')
    letter = letter.upper()
    if 'A' <= letter <= 'Z':
        return ord(letter) - ord('A') + 1
    return -1


def create_problem(problem_name, source_type, templates, source_matching, default_naming=False):
    source_problem_name = problem_name if not default_naming else 'main'
    result = [CreateDirCommand(problem_name)]

    for source_name_pattern in source_matching:
        if source_matching[source_name_pattern] not in templates:
            raise ValueError('There is not template {} for code {}'
                             .format(source_matching[source_name_pattern], source_name_pattern))
        template = templates[source_matching[source_name_pattern]]
        content = template.render(source_problem_name, source_type)
        source_name = re.sub(r'\{\{name\}\}', source_problem_name, source_name_pattern)
        full_path = join(problem_name, source_name)
        os_command = CreateFileCommand(full_path, content)
        result.append(os_command)
        result.append(AddPermissionCommand(full_path))

    return result


def create_contest(contest_name, source_type, problems_count, templates, source_matching):
    result = [CreateDirCommand(contest_name)]

    for i in range(problems_count):
        problem_name = chr(ord('A') + i)
        problem_commands = create_problem(problem_name, source_type, templates, source_matching)
        expanded_problem_commands = [x.expand_path(contest_name) for x in problem_commands]
        result += expanded_problem_commands

    return result


def main(args):
    current_path = os.path.dirname(os.path.realpath(sys.argv[0]))
    templates_path = join(current_path, args.templates_path)
    sources_matching_path = join(current_path, args.sources_matching_path)

    templates = Template.load_all_templates_from_folder(templates_path)
    with open(sources_matching_path, encoding='utf-8') as file:
        sources_matching = json.load(file)

    if args.letter is None:
        os_commands = create_problem(args.name, args.source_type, templates, sources_matching, args.default_naming)
    else:
        problems_count = get_problems_count(args.letter)
        if problems_count == -1:
            raise ValueError('incorrect letter')
        os_commands = create_contest(args.name, args.source_type, problems_count, templates, sources_matching)

    for os_command in os_commands:
        os_command.execute()


if __name__ == '__main__':
    main(parse_args())

__author__ = 'ilyakuchumov'

from os import listdir
from os.path import isfile, join
import re


def get_full_and_short_file_names_in_dir(path):
    full_and_short_names_in_dir = [(join(path, elem_name), elem_name)
                                   for elem_name in listdir(path) if elem_name[0] != '.']
    return [full_and_short_name
            for full_and_short_name in full_and_short_names_in_dir if isfile(full_and_short_name[0])]


class Template:
    def __init__(self, content):
        self.content = content

    @staticmethod
    def load_all_templates_from_folder(path):
        templates = dict()
        for full_name, short_name in get_full_and_short_file_names_in_dir(path):
            with open(full_name, 'r', encoding='utf-8') as file:
                content = file.read()
                templates[short_name] = Template(content)
        return templates

    def render(self, name, code_type):
        code = ''
        for match in re.finditer(r'<(ALL|{0})>\n(.*?)</\1>'.format(code_type), self.content, re.DOTALL):
            template_block = match.group(2)
            code += re.sub(r'\{\{name\}\}', name, template_block)
        return code